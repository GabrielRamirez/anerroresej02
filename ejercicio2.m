
global valor_verdadero cifras es contador x resultado iteracion ea anterior ;

contador = 0; 

disp('=====================================================');
disp('Ejercicio 2: ln(1+x)= x -(x^2)/2 + (x^3)/3 -(x^4)/4 + ...'); 

format long;

cifras=input('¿Cuantas cifras significativas? ');

while(cifras < 0)
disp('Las cifras significativas no pueden ser negativas'); 
cifras = input('Cuantas cifras significativas?');
end

es=(0.5*10^(2-cifras));

disp('El nivel de tolerancia es :');
format short; 
disp(es);
x = input('Ingrese el valor de "x" ');
valor_verdadero = log(1+x);
%disp('iteracion  Respuesta  Error Aprox.')

format long;
resultado =x;
iteracion =1;
ea = 0;

    fprintf('Iteracion          Resultado           Error Aprox \n');
    fprintf(' %.0f                 %g                 %g \n',iteracion,resultado,ea);
    format short;
    anterior = resultado;
    iteracion=iteracion+1;
    contador=contador+1;
    if(mod(iteracion,2)==0)
    resultado = resultado-((x^(iteracion))/(iteracion));
    else
       resultado = resultado+((x^iteracion)/iteracion); 
    end
    ea = (resultado-anterior)/resultado;
    fprintf(' %.0f            %.10f              %.10f \n',iteracion,resultado,ea);
    while(abs(ea)>es && iteracion<40)
    anterior = resultado;
    iteracion=iteracion+1;
    contador=contador+1;
  
    if(mod(iteracion,2)==0)
     resultado = resultado-((x^iteracion)/iteracion);
    else
       resultado = resultado+((x^iteracion)/iteracion); 
    end
    ea = ((resultado-anterior)/resultado)*100;
     fprintf(' %.0f            %.10f              %.10f \n',iteracion,resultado,ea);
    end
    
    if(ea>es)
            disp('El valor real es: ');
            format long; 
            disp(valor_verdadero);
            disp('El nivel de tolerancia es: ');
            format short;
            disp(es);
            disp('La serie converge demasiado lento.');
            disp('resultado: ');
            resultado = '---';
            disp(resultado);
            disp('error: ');
            ea = '---';
            disp(ea);
    else 
    disp('El valor real es: ');
    format long;
    disp(valor_verdadero);
    disp('El nivel de tolerancia es: ');
    disp(es);
    disp('El valor calculado es: ');
    disp(resultado);
    disp('El error es: ');
    format short;
    disp(ea);
    end